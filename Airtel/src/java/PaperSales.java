import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class PaperSales extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Collection outletVector = new Vector();
			Collection cardVector = new Vector();

			ResultSet rs = stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				outletVector.add(rs.getString(1));
				outletVector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",outletVector);

			rs = stmt.executeQuery("select id,cardType from cardtype where cardType not like 'cd%'");
			while(rs.next())
			{
				cardVector.add(rs.getString(1));
				cardVector.add(rs.getString(2));
			}   
			request.setAttribute("cardtypes",cardVector);

			RequestDispatcher rd = request.getRequestDispatcher("/jsp/PaperSales.jsp");
			rd.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class