import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class SubscriberReportProcess extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/AdminActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String mobile = request.getParameter("mobno");
			String msg = "";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");        
			Statement stmt = con.createStatement();

			String docs = "";
			ResultSet rs = stmt.executeQuery("select * from subscriber where mobile='"+mobile+"'");

			if(rs.next())
			{
				String name = rs.getString(2);
				String simcode = rs.getString(4);
				String address = rs.getString(5);
				int document = Integer.parseInt(rs.getString(6));

				rs = stmt.executeQuery("select documentType from documents where id	='"+document+"'");	
				if(rs.next())
				{
					docs = rs.getString(1);
				}
				request.setAttribute("name",name);
				request.setAttribute("mobile",mobile);
				request.setAttribute("simcode",simcode);
				request.setAttribute("address",address);
				request.setAttribute("docs",docs);
				dispatcher = request.getRequestDispatcher("/jsp/SubscriberReportForm.jsp");
				response.setHeader("Content-disposition","attachment; filename=Reports.html" );
				dispatcher.forward(request,response);
			}
			else
			{
				msg = "Subscriber not existed with"+mobile;
				request.setAttribute("msg",msg);
				dispatcher = request.getRequestDispatcher("/jsp/SubReport.jsp");
				dispatcher.forward(request,response);
			}
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class