import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProcessPayments extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
    {
		doPost(request,response);
	}//doPost

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/SimActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			int outid = Integer.parseInt(request.getParameter("outid"));
			int amount = Integer.parseInt(request.getParameter("amt"));

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Timestamp ts = null;
			ResultSet rs = stmt.executeQuery("select payableAmount from outletdetails where id='"+outid+"'");
			if(rs.next())
			{
				int pamt = Integer.parseInt(rs.getString(1));
				if( pamt > 0 )
				{
					pamt -= amount;
					stmt.executeUpdate("update outletdetails set payableAmount='"+pamt+"'where id='"+outid+"'");	
					request.setAttribute("msg", new String("Payable Amount Updated, Balance is:"+pamt+""));
				}
/*				else if( pamt < amount )
				{
					request.setAttribute("msg", new String("Sorry Credit is only'"+pamt+"'"));
				}
*/				else
				{
					request.setAttribute("msg", new String("There is no Credit"));
				}

			}
			Collection vector = new Vector();
			rs = stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				vector.add(rs.getString(1));
				vector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",vector);

			dispatcher = request.getRequestDispatcher("/jsp/Payments.jsp");
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class