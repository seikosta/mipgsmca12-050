import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Category1 extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String catname = request.getParameter("catname");
			String slab = request.getParameter("slab");
			int target = Integer.parseInt(request.getParameter("target"));
			int incentives = Integer.parseInt(request.getParameter("incentives"));
			HttpSession session = request.getSession();
			String id = (String)session.getAttribute("id");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int x = stmt.executeUpdate("update category set name='"+catname+"',slab='"+slab+"',target='"+target+"',insentives='"+incentives+"' where id="+id+"");
			if(x == 1)
			{
				msg = "Category "+catname+" Updated.";
				jspUrl = "/jsp/EditCat.jsp";
			}
			else
			{
				msg = "Problem in updation";
				jspUrl = "/jsp/EditCat.jsp";
			}
			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class