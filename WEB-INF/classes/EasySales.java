import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class EasySales extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Collection vector = new Vector();

			ResultSet rs=stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				vector.add(rs.getString(1));
				vector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",vector);

			RequestDispatcher rd = request.getRequestDispatcher("/jsp/EasySales.jsp");
			rd.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class
