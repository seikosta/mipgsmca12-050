import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class AddOutletDetails extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
			try
		{
			String msg = "";
			String jspUrl = "";
			String outname = request.getParameter("outname");
			String outarea = request.getParameter("outarea");
			int catId = Integer.parseInt(request.getParameter("category"));
			String mob = request.getParameter("mobno");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int id = 0;
			ResultSet rs = stmt.executeQuery("select max(id) from outletdetails");
			if(rs.next())
			{
				if( rs.getString(1) != null )
				{
					id = Integer.parseInt(rs.getString(1));
				}
			}
			id++;
			rs.close();
			rs = stmt.executeQuery(" SELECT mobile FROM outletdetails where mobile='"+mob+"'");
			if( rs.next() == false )
			{ 
				stmt.executeUpdate("insert into outletdetails values('"+id+"','"+outname+"','"+outarea+"','"+mob+"','"+catId+"',0,0)");
				msg = "New Outlet Details Added with Name "+outname;
				jspUrl = "/jsp/outinfo.jsp";
			}
			else
			{
				Collection vector = new Vector();
				rs=stmt.executeQuery("select id,name from category");
				while(rs.next())
				{
					vector.add(rs.getString(1));
					vector.add(rs.getString(2));
				}   
				request.setAttribute("categorynames",vector);
				msg = "Already existed";
				jspUrl = "/jsp/OutletDetails.jsp";
			}

			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class