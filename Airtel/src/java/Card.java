import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Card extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String planname = request.getParameter("planname");
			int amount = Integer.parseInt(request.getParameter("amount"));
			int distprice = Integer.parseInt(request.getParameter("distprice"));

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int id = 0;
			ResultSet rs = stmt.executeQuery("select max(id) from cardtype");
			if(rs.next())
			{
				if( rs.getString(1) != null )
				{
					id = Integer.parseInt(rs.getString(1));
				}
			}
			id++;
			rs.close();
			String cardType = "";
			rs = stmt.executeQuery(" SELECT cardType FROM cardtype where cardType='"+planname+"'");
			if( rs.next() == false )
			{ 
				stmt.executeUpdate("insert into cardtype values('"+id+"','"+planname+"','"+amount+"','"+distprice+"')");
				msg = "Cardtype "+planname+" Updated.";
				jspUrl = "/jsp/Cardtype.jsp";
			}
			else
			{
				cardType = rs.getString(1);
				msg = "Already existed"+cardType;
				jspUrl = "/jsp/Cardtype.jsp";
			}

			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class