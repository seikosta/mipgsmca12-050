import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class UsertReportProcess extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/AdminActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String name = request.getParameter("name");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Timestamp ts = null;
			ts = new Timestamp( (new java.util.Date()).getTime() );
			
			String username = "";
			String fname = "";
			String lname = "";
			String mobno = "";
			int salary = 0;
			ResultSet rs=stmt.executeQuery("select * from users where  userName='"+name+"'");
			if(rs.next())
			{
				username = rs.getString(2);	
				fname = rs.getString(4);
				lname = rs.getString(5);
				mobno = rs.getString(6);
				salary = Integer.parseInt(rs.getString(7));
				request.setAttribute("username",username);
				request.setAttribute("fname",fname);
				request.setAttribute("lname",lname);
				request.setAttribute("mobno",mobno);
				request.setAttribute("salary",salary);
				dispatcher = request.getRequestDispatcher("/jsp/UserReportForm.jsp");
				response.setHeader("Content-disposition","attachment; filename=Reports.html" );	
				dispatcher.forward(request,response);
			}
			else
			{
				request.setAttribute("msg", "Sorry User not exists");
				dispatcher = request.getRequestDispatcher("/jsp/UserReport.jsp");
				dispatcher.forward(request,response);
			}
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class