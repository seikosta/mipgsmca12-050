import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProcessSimActivation extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/SimActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			int outid = Integer.parseInt(request.getParameter("outid"));
			String mobile = request.getParameter("mobile");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int noofacts = 0;
			int subid = 0;

			ResultSet rs = stmt.executeQuery("select noOfActivations from outletdetails where id='"+outid+"'");
			if(rs.next())
			{
				noofacts = Integer.parseInt(rs.getString(1));
			}
			noofacts++;

			rs = stmt.executeQuery("select id from subscriber where mobile like '"+mobile+"'");
			if(rs.next())
			{
				subid = Integer.parseInt(rs.getString(1));
			}
			if( subid != 0 )
			{
				int id = 0;
				String qry = "select * from activationdetails where subscriber = "+subid+"";
				System.out.println(qry);
				ResultSet rs1 = stmt.executeQuery(qry);
				if(rs1.next())
				{
					System.out.println("in already");
					request.setAttribute("msg", new String("Sim Already Activated with mobile no: "+mobile));  
				}
				else
				{
					System.out.println("in new");
					rs = stmt.executeQuery("select max(id) from activationdetails");
					if(rs.next())
					{
						if( rs.getString(1) != null )
						{
							id = Integer.parseInt(rs.getString(1));
						}
					}
					id++;
					Timestamp ts = new Timestamp( (new java.util.Date()).getTime() );
					stmt.executeUpdate("insert into activationdetails values('"+id+"','"+ts+"','"+outid+"','"+subid+"')");
					stmt.executeUpdate("update outletdetails set noOfActivations='"+noofacts+"' where id='"+outid+"'");
					request.setAttribute("msg", new String("Sim Activated with mobile no: "+mobile));  
				}
			}
			else
			{
				request.setAttribute("msg", new String("Subscriber is not available."));    
			}
			Collection vector = new Vector();
			rs = stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				vector.add(rs.getString(1));
				vector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",vector);

			dispatcher = request.getRequestDispatcher("/jsp/SimActivation.jsp");
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class