import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProcessSimSales extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/SimActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			int cardid = Integer.parseInt(request.getParameter("cardid"));
			int count = Integer.parseInt(request.getParameter("count"));
			int outid = Integer.parseInt(request.getParameter("outid"));

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Statement stmtb = con.createStatement();
			Timestamp ts = null;
			String flag = "true";
			int stock1 = 0,id1=0;
			ResultSet ras=stmtb.executeQuery("select count,id from simstock where cardType='"+cardid+"'");
			while(ras.next())
			{
				id1 = ras.getInt(2);
				stock1 = ras.getInt(1);
			}
			ResultSet rs=stmt.executeQuery("select count from simstock where cardType='"+cardid+"'");
			if(rs.next())
			{
				int id = 0;
				int stock = Integer.parseInt(rs.getString(1));
				if(stock1 < count)
				{
					request.setAttribute("msg", new String("Sorry Sim Stock not enough to ur requirement,\n Available stock is : "+stock1));    
				}
				else
				{
					stock1 -= count;

					rs = stmt.executeQuery("select max(id) from simsale");
					if(rs.next())
					{
						if( rs.getString(1) != null )
						{
							id = Integer.parseInt(rs.getString(1));
						}
					}
					id++;
			
					ts = new Timestamp( (new java.util.Date()).getTime() );
					String qry = "update simstock set count='"+stock1+"' where cardType='"+cardid+"' and id="+id1+"";
					System.out.println("+++++++++++"+qry);
					stmt.executeUpdate(qry);	
					String qry1 = "insert into simsale values('"+id+"','"+ts+"','"+count+"','"+outid+"','"+cardid+"','"+flag+"')";
					stmt.executeUpdate(qry1);
					System.out.println("+++++++++++"+qry1);
					request.setAttribute("msg", new String("Sim Stock Updated"));
				}
			}
			else
			{
				request.setAttribute("msg", new String("Sorry Sim Stock not available"));    
			}

			Collection vector = new Vector();
			rs = stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				vector.add(rs.getString(1));
				vector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",vector);
			dispatcher = request.getRequestDispatcher("/jsp/SimSales.jsp");
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class