import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class OutletReportProcess extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;

		try
		{
			int outid=Integer.parseInt(request.getParameter("outid"));
			System.out.println("out name:"+outid);
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			ResultSet rs = null;
			ResultSet rs1 = null;
			rs = stmt.executeQuery("select * from outletdetails where id='"+outid+"'");
			if (rs.next())
			{
				String name = rs.getString(2);
				String area = rs.getString(3);
				String mobile = rs.getString(4);
				int category = Integer.parseInt(rs.getString(5));
				int pbleamt = Integer.parseInt(rs.getString(6));
				int moOfActs = Integer.parseInt(rs.getString(7));
				
				rs1 = stmt.executeQuery("select * from category where id='"+category+"'");	
				if(rs1.next())
				{
					String catname = rs1.getString(2);
					String slab = rs1.getString(3);
					int target = Integer.parseInt(rs1.getString(4));
					int incentives = Integer.parseInt(rs1.getString(5));

					request.setAttribute("name",name);
					request.setAttribute("area",area);
					request.setAttribute("catname",catname);
					request.setAttribute("slab",slab);
					request.setAttribute("target",target);
					request.setAttribute("incentives",incentives);
					request.setAttribute("mobile",mobile);
					request.setAttribute("pbleamt",pbleamt);
					dispatcher = request.getRequestDispatcher("/jsp/OutletReportForm.jsp");
					response.setHeader("Content-disposition","attachment; filename=Reports.html" );
					dispatcher.forward(request,response);
				}
			}
			else
			{
				Collection vector = new Vector();
				rs = stmt.executeQuery("select id,name from outletdetails");
				while(rs.next())
				{
					vector.add(rs.getString(1));
					vector.add(rs.getString(2));
				}   
				request.setAttribute("outletnames",vector);
				request.setAttribute("msg",new String("Outlet Details does not exist"));
				dispatcher = request.getRequestDispatcher("/jsp/OutletReport.jsp");
				dispatcher.forward(request,response);
			}		
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class