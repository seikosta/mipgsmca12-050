import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class CreditReportProcess extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/AdminActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			int outletid = Integer.parseInt(request.getParameter("outid"));
			String date1 = request.getParameter("collectdate");
			String date2 = request.getParameter("collectdate1");
			String d = date1.substring(0,2);
/*			int dd = Integer.parseInt(d);dd--;
			d = dd+"";
*/			String m = date1.substring(3,5);
			String y = date1.substring(6,10); 
			String indate = y + "-" + m + "-" + d;
			System.out.println("Date::"+indate);
			String d1 = date2.substring(0,2);
/*			int dd1 = Integer.parseInt(d1);dd1++;
			d1 = dd1+"";
*/			String m1 = date2.substring(3,5);
			String y1 = date2.substring(6,10); 
			String indate1 = y1 + "-" + m1 + "-" + d1;
			System.out.println("Date::"+indate1);
		



			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Timestamp ts = null;
			ts = new Timestamp( (new java.util.Date()).getTime() );
			
			int distprice = 0;
			int simcredit = 0;
			int easycredit = 0;
			int papercredit = 0;
			int totalcredit = 0;

			String flag1 = "true";
			String flag2 = "true";
			String flag3 = "true";
			String a = "select count,cardType from simsale where  outlet='"+outletid+"' and flag='"+flag1+"' and timestamp>'"+indate+"' or timestamp='"+indate+"' and timestamp<'"+indate1+"' or timestamp='"+indate1+"'";
			System.out.println("aaaaaaaa"+a);
			ResultSet rs=stmt.executeQuery(a);
			while(rs.next())
			{
				flag1 = "false";
				int count = rs.getInt(1);
				int cardType = rs.getInt(2);
				
				rs = stmt.executeQuery("select distPrice from cardtype where id='"+cardType+"'");	
				if(rs.next())
				{
					distprice = Integer.parseInt(rs.getString(1));
					simcredit += count * distprice;
				}
			}
			if( flag1 == "false" )
			{
				stmt.executeUpdate("update simsale set flag='"+flag1+"' where outlet='"+outletid+"'");
			}
			String b = "select count,cardType from papersale where outlet='"+outletid+"' and flag='"+flag2+"' and timestamp>='"+indate+"' and timestamp<='"+indate1+"'";
			System.out.println("bbbbbbbbbb"+b);
			rs = stmt.executeQuery(b);
			while(rs.next())
			{
				flag2 = "false";
				int count = rs.getInt(1);
				int cardType = rs.getInt(2);
				
				rs = stmt.executeQuery("select distPrice from cardtype where id='"+cardType+"'");	
				if(rs.next())
				{
					distprice = Integer.parseInt(rs.getString(1));
					papercredit += count * distprice;
				}
			}
			if( flag2 == "false" )
			{
				stmt.executeUpdate("update papersale set flag='"+flag2+"' where outlet='"+outletid+"'");
			}
				String c = "select amount from easysales where outlet='"+outletid+"' and flag='"+flag3+"' and timestamp>='"+indate+"' and timestamp<='"+indate1+"'";
				System.out.println(c);
			rs = stmt.executeQuery(c);
			while(rs.next())
			{
				flag3 = "false";
				int amount = Integer.parseInt(rs.getString(1));
				easycredit += amount;
			}
			if( flag3 == "false" )
			{
				stmt.executeUpdate("update easysales set flag='"+flag3+"' where outlet='"+outletid+"'");
			}

			int payamount = 0;
			String outname = ""	;
			rs = stmt.executeQuery("select name,payableAmount from outletdetails where id='"+outletid+"'");
			if(rs.next())
			{
				outname = rs.getString(1);
				payamount = rs.getInt(2);
			}
			totalcredit = simcredit + easycredit + papercredit + payamount;
			stmt.executeUpdate("update outletdetails set payableAmount='"+totalcredit+"' where name='"+outname+"'");

			if(flag1 == "false" && flag2 == "false" && flag3 == "false" && totalcredit == 0)
			{
				request.setAttribute("msg", new String("Sorry there is no credit to this outlet"));

				Collection vector = new Vector();
				rs = stmt.executeQuery("select id,name from outletdetails");
				while(rs.next())
				{
					vector.add(rs.getString(1));
					vector.add(rs.getString(2));
				}   
				request.setAttribute("outletnames",vector);
				dispatcher = request.getRequestDispatcher("/jsp/CreditReport.jsp");
				dispatcher.forward(request,response);
			}
			else
			{
				request.setAttribute("sim",simcredit);
				request.setAttribute("easy",easycredit);
				request.setAttribute("paper",papercredit);
				request.setAttribute("total",totalcredit);
				request.setAttribute("name",outname);
				dispatcher = request.getRequestDispatcher("/jsp/CreditReportForm.jsp");
				response.setHeader("Content-disposition","attachment; filename=Reports.html" );
				dispatcher.forward(request,response);
			}
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class