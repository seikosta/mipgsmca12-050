import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProcessEasySales extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
    {
		doPost(request,response);
	}//doPost

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/SimActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			int outid = Integer.parseInt(request.getParameter("outid"));
			int amount = Integer.parseInt(request.getParameter("easyamt"));

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Statement stmtb = con.createStatement();
			Timestamp ts = null;
			int stock1=0,id1=0;
			ResultSet ras=stmtb.executeQuery("select amount,id from easystock where amount>0");
			while(ras.next())
			{
				id1 = ras.getInt(2);
				stock1 = ras.getInt(1);
                System.out.println("Available amount:"+stock1);
			}

			ResultSet rs = stmt.executeQuery("select amount,id from easystock ");
			if(stock1 > 0)
			{
				String flag = "true";
					//int amt = rs.getInt(1);
				System.out.println("amt is:"+amount);
				System.out.println("Available stock is:"+stock1);
				
				
				if(stock1 < amount)
				{
					request.setAttribute("msg", new String("Sorry Easy Rc. Amount not enough to ur requirement,\n Available Amount is : "+stock1));    
				}
				else
				{
					int stock2 = stock1; 
					stock2 -= amount;

					int id = 0;
					rs = stmt.executeQuery("select max(id) from easysales");
					if(rs.next())
					{
						if( rs.getString(1) != null )
						{
							id = Integer.parseInt(rs.getString(1));
						}
					}
					id++;
					ts = new Timestamp( (new java.util.Date()).getTime() );
					String q1 = "update easystock set timestamp='"+ts+"', amount='"+stock2+"' where id="+id1+"";
					System.out.println("Q1:"+q1);
					String q2 = "insert into easysales values('"+id+"','"+ts+"','"+amount+"','"+outid+"','"+flag+"')";
										System.out.println("Q2:"+q2);
					stmt.executeUpdate(q1);	
					stmtb = con.createStatement();
					stmtb.executeUpdate(q2);
					request.setAttribute("msg", new String("Easy Rc. amount Updated"));
					
				}
			}
			else
			{
				request.setAttribute("msg", new String("Sorry Easy Rc. amount not available"));    
			}
			
			Collection vector = new Vector();
			rs = stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				vector.add(rs.getString(1));
				vector.add(rs.getString(2));
			}   
			request.setAttribute("outletnames",vector);

			dispatcher = request.getRequestDispatcher("/jsp/EasySales.jsp");
			dispatcher.forward(request,response);
        stmt.close();
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class