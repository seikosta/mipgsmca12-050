import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Documents1 extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String doctype = request.getParameter("doctype");
			HttpSession session = request.getSession();
			String id = (String)session.getAttribute("id");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int s = stmt.executeUpdate("update documents set documentType='"+doctype+"' where id='"+id+"'");
			if( s == 1 )
			{ 
				msg = "Document Updated Successfully";
				jspUrl = "/jsp/EditProofs.jsp";
			}
			else
			{
				msg = "Problem in Updation!!";
				jspUrl = "/jsp/EditProofs.jsp";
			}

			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);


		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class