import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class AddSubscriber2 extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String subname = request.getParameter("subname");
			String mobno = request.getParameter("mobno");
			String simId = request.getParameter("simid");
			String address = request.getParameter("address");
			int docid = Integer.parseInt(request.getParameter("doctype"));
			Collection vector = new Vector();

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			HttpSession session = request.getSession();
			String id = (String)session.getAttribute("id");
			int i =	stmt.executeUpdate("update subscriber set name='"+subname+"',mobile='"+mobno+"',simCode='"+simId+"',address='"+address+"',document='"+docid+"' where id='"+id+"'");
			if(i == 1)
			{
				msg = "Subscriber Details Updated Successfully";
				jspUrl = "/jsp/EditSubscriber.jsp";
			}
			else
			{
				msg = "Problem in Updation";
				jspUrl = "/jsp/EditSubscriber.jsp";
			}
			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class