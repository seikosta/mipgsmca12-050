import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class SimStock extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
		RequestDispatcher dispatcher = null;
		dispatcher = request.getRequestDispatcher("/jsp/SimStock.jsp");
		dispatcher.forward(request,response);
	}//doPost
}//class