import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class PaperSale extends HttpServlet
{
	public void doGet(HttpServletRequest req,HttpServletResponse resp)throws ServletException,IOException
	{
		doPost(req,resp);
	}//doGet

	public void doPost(HttpServletRequest req,HttpServletResponse resp)throws ServletException,IOException
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			Collection outletVector=new Vector();
			Collection cardtypeVector=new Vector();

			ResultSet rs=stmt.executeQuery("select id,name from outletdetails");
			while(rs.next())
			{
				outletVector.add(rs.getString(1));
				outletVector.add(rs.getString(2));
			}   
			req.setAttribute("outletnames",outletVector);

			rs=stmt.executeQuery("select id,cardType from cardtype");
			while(rs.next())
			{
				cardtypeVector.add(rs.getString(1));
				cardtypeVector.add(rs.getString(2));
			}   
			req.setAttribute("cardtypes",cardtypeVector);

			RequestDispatcher rd=req.getRequestDispatcher("/jsp/PaperSales.jsp");
			rd.forward(req,resp);
		}//try
		catch(Exception e)
		{
			e.printStackTrace();
		}//catch
	}//doPost
}//class