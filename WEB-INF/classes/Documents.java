import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Documents extends HttpServlet
{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String doctype = request.getParameter("doctype");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int id = 0;
			ResultSet rs = stmt.executeQuery("select max(id) from documents");
			if(rs.next())
			{
				if( rs.getString(1) != null )
				{
					id = Integer.parseInt(rs.getString(1));
				}
			}
			id++;
			rs.close();
			rs = stmt.executeQuery(" SELECT documentType FROM documents where documentType='"+doctype+"'");
			if( rs.next() == false )
			{ 
				stmt.executeUpdate("insert into documents values('"+id+"','"+doctype+"')");
				msg = "New Document Added";
				jspUrl = "/jsp/Documents.jsp";
			}
			else
			{
				msg = "Already existed";
				jspUrl = "/jsp/Documents.jsp";
			}

			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);


		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class