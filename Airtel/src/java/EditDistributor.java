import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class EditDistributor extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		doPost(request,response);
	}//doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
		RequestDispatcher dispatcher = null;
		String event = request.getParameter("eventAction");
		if( event.equals("exit") )
		{
			dispatcher = request.getRequestDispatcher("/jsp/OperatorActivities.jsp");
			dispatcher.forward(request,response);
		}
		try
		{
			String msg = "";
			String jspUrl = "";
			String distcode = request.getParameter("distcode");
			String glcode = request.getParameter("glcode");
			String region = request.getParameter("region");
			String district = request.getParameter("district");
			String hub = request.getParameter("hub");
			String address = request.getParameter("address");
			HttpSession session = request.getSession();
			String id1 = (String)session.getAttribute("id");
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airtel","root","root");
			Statement stmt = con.createStatement();
			int id = 0;
			
			int k = stmt.executeUpdate("update distributor set code='"+distcode+"',glcode='"+glcode+"',address='"+address+"',region='"+region+"',district='"+district+"',hub='"+hub+"' where id='"+id1+"'");
			if(k == 1)
			{
				msg = "Distributor Details Updated Successfully";
				jspUrl = "/jsp/EditDistributor.jsp";
			}
			else
			{
				msg = "Problem in Updation";
				jspUrl = "/jsp/EditDistributor.jsp";
			}
			System.out.println(msg);
			request.setAttribute("msg",msg);
			dispatcher = request.getRequestDispatcher(jspUrl);
			dispatcher.forward(request,response);
		}//try
		catch(Exception e)
		{
            e.printStackTrace();
		}//catch
	}//doPost
}//class